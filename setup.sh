export AtlasSetup=/afs/cern.ch/atlas/software/dist/AtlasSetup
alias asetup='source $AtlasSetup/scripts/asetup.sh'
# asetup 22.0.0,Athena
# asetup 22.0.1,Athena
asetup 22.0.14,Athena
# asetup 22.0.20,Athena
# asetup master,latest,Athena
